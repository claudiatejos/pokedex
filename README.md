# Welcome to the Pokedex

## In this project you will be able to:

- View all the Pokemon on a list and grid view.
- Search for Pokemon by text
- Filter Pokemon by types
- Favorite/Unfavorite a Pokemon
- Switch between All Pokemon and Favorite Pokemon views
- View Pokemon details

#### You can check it out [here](https://fantastic-strudel-58c801.netlify.app/)


## About the technologies:

- Based in `React` with `Typescript`
- **Meta-framkeworks** it's used `Remix run`
- Component library used `Chakra UI`
- Icons library used `Chakra Icons` and `React Icons`

### Technologies used for testing:
- Component test it's used `jest, react testing library`
- E2E test it's used `cypress`


### Where is it hosted:
- Netlify

## How I decided on the technologies:

For Remix run I have heard about it, and wanted to have some time to try it out, and why not at this moment and challenge myself? So I studied it a bit and started the project, along with it, was improving myself on the framework.

React, is part of my daily activities, Chakra in the other hand, I also have never it used before, but have heard alot about it, so wanted to give it a try so I could have my own opinion on it.

And for tests, those are also on my daily activities and I really like those.

## How much I knew the used technologies:

Some of those technologies were new to me, for example Remix, so I had to learn it a bit before starting this project, Chakra UI has also been the first time working with it.

But others like React, jest, react testing library, cypress are part of my daily job. Typescript is not part of my daily job, but have a small experience with it, so I was able to improve myself on it.
