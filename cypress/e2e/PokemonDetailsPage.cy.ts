describe('Pokemon details page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Validate favorite', () => {
    cy.fixture('singlePokemonDetail').then((pokemonDetail) => {
      // Intercept call
      cy.intercept('GET', '/001?_data=routes%2F%24pokemonId', pokemonDetail).as(
        'firstDetailsFetch'
      );

      cy.get('[data-testid="grid-item"]').first().click();
      cy.wait('@firstDetailsFetch');
      cy.url().should('include', '001');
      //   so we can be sure we are on details page
      cy.get('body').contains('Types');
      cy.get('body').contains(pokemonDetail.name);

      // Check current pokemon status
      const pokemonFavoriteIcon = cy.get(
        '[data-testid="detail-icon-favorite"]'
      );
      pokemonFavoriteIcon.should('have.css', 'color', 'rgb(74, 85, 104)');

      //   Intercept another GET and POST request
      cy.intercept('POST', '/001?_data=routes%2F%24pokemonId', {
        ...pokemonDetail,
        isFavorite: true,
      }).as('favoritePokemon');

      cy.intercept('GET', '/001?_data=routes%2F%24pokemonId', {
        ...pokemonDetail,
        isFavorite: true,
      }).as('secondDetailsFetch');

      //   Click in favorite
      pokemonFavoriteIcon.click();
      cy.wait('@favoritePokemon');
      cy.wait('@secondDetailsFetch');

      cy.get('[data-testid="detail-icon-favorite"]').should(
        'have.css',
        'color',
        'rgb(229, 62, 62)'
      );
    });
  });
  it('Validate unfavorite', () => {
    cy.fixture('singlePokemonDetail').then((pokemonDetail) => {
      // Intercept call
      cy.intercept('GET', '/001?_data=routes%2F%24pokemonId', {
        ...pokemonDetail,
        isFavorite: true,
      }).as('firstDetailsFetch');

      cy.get('[data-testid="grid-item"]').first().click();
      cy.wait('@firstDetailsFetch');
      cy.url().should('include', '001');
      //   so we can be sure we are on details page
      cy.get('body').contains('Types');
      cy.get('body').contains(pokemonDetail.name);

      // Check current pokemon status
      const pokemonFavoriteIcon = cy.get(
        '[data-testid="detail-icon-favorite"]'
      );
      pokemonFavoriteIcon.should('have.css', 'color', 'rgb(229, 62, 62)');

      //   Intercept another GET and POST request
      cy.intercept('POST', '/001?_data=routes%2F%24pokemonId', {
        ...pokemonDetail,
        isFavorite: false,
      }).as('unfavoritePokemon');

      cy.intercept('GET', '/001?_data=routes%2F%24pokemonId', {
        ...pokemonDetail,
        isFavorite: false,
      }).as('secondDetailsFetch');

      //   Click to unfavorite
      pokemonFavoriteIcon.click();
      cy.wait('@unfavoritePokemon');
      cy.wait('@secondDetailsFetch');

      cy.get('[data-testid="detail-icon-favorite"]').should(
        'have.css',
        'color',
        'rgb(74, 85, 104)'
      );
    });
  });
  it('Pokemon with evolutions', () => {
    cy.fixture('singlePokemonDetail').then((pokemonDetail) => {
      // Intercept call
      cy.intercept('GET', '/001?_data=routes%2F%24pokemonId', pokemonDetail).as(
        'firstDetailsFetch'
      );

      cy.get('[data-testid="grid-item"]').first().click();

      cy.wait('@firstDetailsFetch');
      cy.url().should('include', '001');
      cy.get('body').contains('Evolutions');

      pokemonDetail.evolutions.forEach((evolution) => {
        cy.get(`[data-testid="${evolution.id}-${evolution.name}"]`).contains(
          evolution.name
        );
      });
    });
  });
  it('Pokemon with evolutions  and previous evolutions - Ivysaur', () => {
    cy.fixture('ivysaurDetails').then((ivysaurDetail) => {
      // Intercept call
      cy.intercept('GET', '/002?_data=routes%2F%24pokemonId', ivysaurDetail).as(
        'firstDetailsFetch'
      );

      cy.get('[data-testid="grid-item"]').eq(1).click();

      cy.wait('@firstDetailsFetch');
      cy.url().should('include', '002');
      cy.get('body').contains('Evolutions');

      ivysaurDetail.evolutions.forEach((evolution) => {
        cy.get(`[data-testid="${evolution.id}-${evolution.name}"]`).contains(
          evolution.name
        );
      });
      ivysaurDetail.previousEvolutions.forEach((evolution, idx) => {
        cy.get(`[data-testid="${evolution.id}-${evolution.name}"]`).contains(
          evolution.name
        );
        if (idx === ivysaurDetail.previousEvolutions.length - 1) {
          // Last item lets test the click buttons
          cy.get(`[data-testid="${evolution.id}-${evolution.name}"]`).click();
          cy.wait(1000);
          cy.url().should('include', evolution.id);
          cy.get('body').contains(evolution.name);
        }
      });
    });
  });
  it('Click back button', () => {
    // Intercept call
    cy.intercept('GET', '/003?_data=routes%2F%24pokemonId').as('detailsFetch');
    cy.get('[data-testid="grid-item"]').eq(2).click();
    cy.wait('@detailsFetch');
    cy.url().should('include', '003');
    cy.get('[data-testid="back-button"]').click();
    cy.url().should('include', '/');
    cy.get('[data-testid="grid-item"]').should('have.length', 10);
  });
  it('Page not found - render error', () => {
    cy.visit('http://localhost:3000/199191/9191919', {
      failOnStatusCode: false,
    });

    cy.get('body').contains('Page not found');
  });
});
