describe('Pokedex - Grid view', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Initialize with grid selected and all selected', () => {
    // Get the button `All` and validate is the default selected
    cy.get('[data-testid="all-pokemons"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    // Get the button `Favorite` and validate it's  with selected color
    cy.get('[data-testid="favorite-pokemons"]').should(
      'not.have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    cy.get('[data-testid="grid-item"]').should('have.length', 10);
  });
  it('Validate favorite state in grid', () => {
    // intercept GET
    cy.intercept('GET', '/?_data=routes%2Findex', {
      fixture: 'pokemons.json',
    }).as('firstPokemonLoad');

    // Load fixtured so we can intercept POST and second GET
    cy.fixture('pokemons.json').then((pokemons) => {
      //   we know from fixtures the state of first item
      const firstPokemonFixture = pokemons.data.items[0];

      const favoriteReturn = {
        ...firstPokemonFixture,
        isFavorite: !firstPokemonFixture.isFavorite,
      };

      cy.intercept('GET', '/?_data=routes%2Findex', {
        ...pokemons,
        data: {
          ...pokemons.data,
          items: [
            favoriteReturn,
            ...pokemons.data.items.filter((_, idx: number) => idx !== 0),
          ],
        },
      }).as('pokemonsAfterClickFavorite');

      cy.intercept('POST', '/?index=&_data=routes%2Findex', favoriteReturn).as(
        'changeFavorite'
      );

      // First pokemon before change status
      const firstPokemon = cy.get('[data-testid="icon-favorite"]').first();

      firstPokemon.should(
        'have.css',
        'color',
        firstPokemonFixture.isFavorite ? 'rgb(229, 62, 62)' : 'rgb(74, 85, 104)'
      );
      firstPokemon.click();

      // wait for intercept
      cy.wait('@firstPokemonLoad');
      cy.wait('@pokemonsAfterClickFavorite');
      cy.wait(500);

      //   Get fist pokemon again to check it's favorite state
      const firstPokemonChangeState = cy
        .get('[data-testid="icon-favorite"]')
        .first();

      firstPokemonChangeState.should(
        'have.css',
        'color',
        !firstPokemonFixture.isFavorite
          ? 'rgb(229, 62, 62)'
          : 'rgb(74, 85, 104)'
      );
    });
  });
  it('Select pokemon - should take to pokemons detail', () => {
    cy.get('[data-testid="grid-item"]').first().click();
    cy.url({ timeout: 1000 }).should('include', '001');
  });
});

describe('Pokedex - List view', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Set list view', () => {
    // Select list view
    cy.get('[data-testid="show-list-view"]').click();
    // Validate it's with selected mode
    cy.get('[data-testid="show-list-view"]', { timeout: 1000 }).should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    cy.get('[data-testid="list-item"]').should('have.length', 10);
  });
  it('Validate favorite state in list', () => {
    // intercept GET
    cy.intercept('GET', '/?_data=routes%2Findex', {
      fixture: 'pokemons.json',
    }).as('firstPokemonLoad');
    cy.get('[data-testid="show-list-view"]').click();

    cy.fixture('pokemons').then((pokemons) => {
      const pokemonIndex = Math.floor(
        Math.random() * pokemons.data.items.length
      );

      const pokemonData = pokemons.data.items[pokemonIndex];

      // check pokemons status
      const favoriteIconForPokemonIndex = cy
        .get('[data-testid="icon-favorite"]')
        .eq(pokemonIndex);

      favoriteIconForPokemonIndex.should(
        'have.css',
        'color',
        pokemonData.isFavorite ? 'rgb(229, 62, 62)' : 'rgb(74, 85, 104)'
      );

      // click favorite/unfavorite
      favoriteIconForPokemonIndex.click();

      // intercept POST and second GET
      cy.intercept('POST', '/?index=&_data=routes%2Findex', {
        ...pokemonData,
        isFavorite: !pokemonData.isFavorite,
      }).as('postFavorite');

      cy.intercept('GET', '/?_data=routes%2Findex', {
        ...pokemons,
        data: {
          ...pokemons.data,
          items: pokemons.data.items.map((i, idx: number) =>
            idx !== pokemonIndex
              ? i
              : { ...pokemonData, isFavorite: !pokemonData.isFavorite }
          ),
        },
      }).as('getAfterClickFavoriteIcon');

      cy.wait('@firstPokemonLoad');
      cy.wait('@getAfterClickFavoriteIcon');
      cy.wait(1000);

      // check pokemon status
      cy.get('[data-testid="icon-favorite"]')
        .eq(pokemonIndex)
        .should(
          'have.css',
          'color',
          !pokemonData.isFavorite ? 'rgb(229, 62, 62)' : 'rgb(74, 85, 104)'
        );
    });
  });
  it('Select pokemon - should take to pokemons details', () => {
    cy.get('[data-testid="show-list-view"]').click();

    cy.get('[data-testid="list-item"]', { timeout: 1000 }).first().click();
    cy.url({ timeout: 1000 }).should('include', '001');
  });
});

describe('Pokedex - favorite tab', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Select favorite in grid mode', () => {
    cy.fixture('favoritePokemons').then((favoritesPokemons) => {
      cy.intercept(
        'GET',
        '/?isFavorite=true&page=1&_data=routes%2Findex',
        favoritesPokemons
      ).as('favorites');

      cy.get('[data-testid="favorite-pokemons"]').click();

      cy.wait('@favorites');
      cy.url().should('include', 'isFavorite=true');

      cy.get('[data-testid="favorite-pokemons"]').should(
        'have.css',
        'color',
        'rgb(72, 187, 120)'
      );

      // should have the same count
      cy.get('[data-testid="grid-item"]').should(
        'have.length',
        favoritesPokemons.data.count
      );
      // should all have the favorite heart icon in red
      cy.get('[data-testid="icon-favorite"]').should(
        'have.css',
        'color',
        'rgb(229, 62, 62)'
      );

      // Favorites from API should be the ones in the UI
      favoritesPokemons.data.items.forEach((item) => {
        cy.get('[data-testid="grid-item"] div div').contains(item.name);
      });
    });
  });
  it('Unfavorite pokemon in grid mode - should be removed from grid', () => {
    cy.fixture('favoritePokemons').then((favoritesPokemons) => {
      const pokemonIndexToBeRemoved = Math.floor(
        Math.random() * favoritesPokemons.data.items.length
      );
      const pokemonData = favoritesPokemons.data.items[pokemonIndexToBeRemoved];

      const unfavoritePokemon = {
        ...pokemonData,
        isFavorite: !pokemonData.isFavorite,
      };
      cy.intercept(
        'GET',
        '/?isFavorite=true&page=1&_data=routes%2Findex',
        favoritesPokemons
      ).as('favorites');

      cy.get('[data-testid="favorite-pokemons"]').click();
      cy.wait('@favorites');

      cy.intercept(
        'POST',
        '/?index=&isFavorite=true&page=1&_data=routes%2Findex',
        unfavoritePokemon
      ).as('unfavoritePokemon');

      cy.intercept('GET', '/?isFavorite=true&page=1&_data=routes%2Findex', {
        ...favoritesPokemons,
        data: {
          ...favoritesPokemons.data,
          items: [
            ...favoritesPokemons.data.items.filter(
              (_, idx: number) => idx !== pokemonIndexToBeRemoved
            ),
          ],
        },
      }).as('favoritesAfterUnfavorite');

      cy.get('[data-testid="grid-item"] div div').contains(pokemonData.name);

      // remove first from list
      cy.get('[data-testid="icon-favorite"]')
        .eq(pokemonIndexToBeRemoved)
        .click();

      cy.wait('@unfavoritePokemon');
      cy.wait('@favoritesAfterUnfavorite');
      cy.wait(1000);

      //   Make sure pokemon is not in the list anymore
      cy.get('[data-testid="grid-item"] div div').each(($el) => {
        cy.wrap($el).should('not.have.text', pokemonData.name);
      });
    });
  });
  it('Select favorite in list mode', () => {
    cy.fixture('favoritePokemons').then((favoritesPokemons) => {
      cy.intercept(
        'GET',
        '/?isFavorite=true&page=1&_data=routes%2Findex',
        favoritesPokemons
      ).as('favorites');

      cy.get('[data-testid="favorite-pokemons"]').click();

      // Click in list/table view and validate it's selected
      cy.get('[data-testid="show-list-view"]').click();
      cy.get('[data-testid="show-list-view"]').should(
        'have.css',
        'color',
        'rgb(72, 187, 120)'
      );

      cy.wait('@favorites');
      cy.url().should('include', 'isFavorite=true');

      cy.get('[data-testid="favorite-pokemons"]').should(
        'have.css',
        'color',
        'rgb(72, 187, 120)'
      );

      // should have the same count
      cy.get('[data-testid="list-item"]').should(
        'have.length',
        favoritesPokemons.data.count
      );
      // should all have the favorite heart icon in red
      cy.get('[data-testid="icon-favorite"]').should(
        'have.css',
        'color',
        'rgb(229, 62, 62)'
      );

      // Favorites from API should be the ones in the UI
      favoritesPokemons.data.items.forEach((item) => {
        cy.get('[data-testid="list-item"] td div').contains(item.name);
      });
    });
  });
  it('Unfavorite pokemon in list mode - should be removed from list', () => {
    cy.fixture('favoritePokemons').then((favoritesPokemons) => {
      const pokemonIndexToBeRemoved = Math.floor(
        Math.random() * favoritesPokemons.data.items.length
      );

      const pokemonData = favoritesPokemons.data.items[pokemonIndexToBeRemoved];

      const unfavoritePokemon = {
        ...pokemonData,
        isFavorite: !pokemonData.isFavorite,
      };

      cy.intercept(
        'GET',
        '/?isFavorite=true&page=1&_data=routes%2Findex',
        favoritesPokemons
      ).as('favorites');

      cy.get('[data-testid="favorite-pokemons"]').click();
      cy.wait('@favorites');
      cy.get('[data-testid="show-list-view"]').click();

      cy.intercept(
        'POST',
        '/?index=&isFavorite=true&page=1&_data=routes%2Findex',
        unfavoritePokemon
      ).as('unfavoritePokemon');

      cy.intercept('GET', '/?isFavorite=true&page=1&_data=routes%2Findex', {
        ...favoritesPokemons,
        data: {
          ...favoritesPokemons.data,
          items: [
            ...favoritesPokemons.data.items.filter(
              (_, idx: number) => idx !== pokemonIndexToBeRemoved
            ),
          ],
        },
      }).as('favoritesAfterUnfavorite');

      cy.get('[data-testid="list-item"] td div').contains(pokemonData.name);

      // remove first from list
      cy.get('[data-testid="icon-favorite"]')
        .eq(pokemonIndexToBeRemoved)
        .click();

      cy.wait('@unfavoritePokemon');
      cy.wait('@favoritesAfterUnfavorite');
      cy.wait(1000);

      //   Make sure pokemon is not in the list anymore
      cy.get('[data-testid="list-item"] td div').each(($el) => {
        cy.wrap($el).should('not.have.text', pokemonData.name);
      });
    });
  });
  it('No pokemons favorite', () => {
    cy.intercept('GET', '/?isFavorite=true&page=1&_data=routes%2Findex', {
      statusCode: 404,
    }).as('noPokemonsFavorited');
    cy.get('[data-testid="favorite-pokemons"]').click();
    cy.wait('@noPokemonsFavorited');
    cy.get('body').contains('Pokemon not found');
  });
});

describe('Pokedex - search and filter', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Search', () => {
    const searchType = 'Mac';

    cy.intercept({
      method: 'GET',
      url: `/?search=${searchType}&page=1&_data=routes%2Findex`,
    }).as('searchURL');

    cy.intercept({
      method: 'GET',
      url: '/?page=1&_data=routes%2Findex',
    }).as('basicFetch');

    cy.get('[data-testid="search-input"]').type(searchType);
    cy.wait('@searchURL');
    cy.url().should('include', `search=${searchType}`);
    cy.get('[data-testid="grid-item"] div div', { timeout: 1000 }).contains(
      searchType
    );

    cy.get('[data-testid="search-input"]').clear();

    cy.wait('@basicFetch');
    cy.url().should('not.include', `search=${searchType}`);
  });
  it('Filter by type', () => {
    const filterType = 'Poison';

    cy.intercept({
      method: 'GET',
      url: `/?type=${filterType}&page=1&_data=routes%2Findex`,
    }).as('filterByTypeURL');

    cy.get('[data-testid="select-input"]').select(filterType);

    cy.url().should('include', `type=${filterType}`);
    cy.get('[data-testid="select-input"]').contains(filterType);

    cy.wait('@filterByTypeURL');
    cy.wait(1000);
    cy.get('[data-testid="select-input"]').select('Select type');

    cy.url().should('not.include', `type=${filterType}`);
  });
  it('Search and filter', () => {
    const searchType = 'Pi';
    const filterType = 'Normal';

    cy.intercept({
      method: 'GET',
      url: `/?search=${searchType}&page=1&_data=routes%2Findex`,
    }).as('searchURL');
    cy.intercept({
      method: 'GET',
      url: `/?search=${searchType}&page=1&type=${filterType}&_data=routes%2Findex`,
    }).as('searchAndTypeURL');

    cy.get('[data-testid="search-input"]').type(searchType);
    cy.wait('@searchURL');
    cy.url().should('include', `search=${searchType}`);

    cy.get('[data-testid="select-input"]').select(filterType);
    cy.wait('@searchAndTypeURL');
    cy.url()
      .should('include', `search=${searchType}`)
      .should('include', `type=${filterType}`);
  });
});

describe('Pokedex - pagination', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });
  it('Validate pagination - default', () => {
    //   Page 1 should be the default selected
    cy.get('[data-testid="pagination-1"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    // Previous page should not be allowed
    cy.get('[data-testid="previous-page"]').should(
      'have.css',
      'cursor',
      'not-allowed'
    );

    // Select another page
    cy.get('[data-testid="pagination-3"]').click();
    cy.url({ timeout: 1000 }).should('include', 'page=3');
    cy.get('[data-testid="pagination-3"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    // Check we can go further to next page and back to previous page
    cy.get('[data-testid="previous-page"]').click();
    cy.url({ timeout: 1000 }).should('include', 'page=2');
    cy.get('[data-testid="pagination-2"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    cy.get('[data-testid="next-page"]').click();
    cy.url({ timeout: 1000 }).should('include', 'page=3');
    cy.get('[data-testid="pagination-3"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );
  });
  it('Validate pagination - search', () => {
    const searchInput = 'Ni';
    cy.get('[data-testid="search-input"]').type(searchInput);
    cy.url({ timeout: 1000 }).should('include', `search=${searchInput}`);
    cy.wait(2000);
    cy.get('[data-testid="pagination-1"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );
  });
  it('Validate pagination - mid pagination do search', () => {
    //   Test search being in mid of pagination
    cy.get('[data-testid="pagination-3"]').click();
    cy.url({ timeout: 2000 }).should('include', 'page=3');
    const searchInput = 'Ni';
    cy.get('[data-testid="search-input"]').type(searchInput);
    cy.url({ timeout: 1000 })
      .should('include', `search=${searchInput}`)
      .should('include', 'page=1');
  });
  it('Validate pagination - filter', () => {
    const filterType = 'Water';

    cy.intercept({
      method: 'GET',
      url: `/?type=${filterType}&page=1&_data=routes%2Findex `,
    }).as('filterByTypeURL');

    cy.get('[data-testid="select-input"]').select(filterType);
    cy.wait('@filterByTypeURL').wait(1000);

    cy.get('[data-testid="pagination-1"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );

    // we know we have 4 pages in Water types
    cy.get('[data-testid="pagination-4"]').click();
    cy.url({ timeout: 1000 }).should('include', 'page=4');
    cy.get('[data-testid="pagination-4"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );
  });
  it('Validate pagination - favorite', () => {
    cy.intercept('GET', '/?isFavorite=true&page=1&_data=routes%2Findex', {
      fixture: 'favoritePokemonsMany.json',
    }).as('favoriteData');

    cy.get('[data-testid="favorite-pokemons"]').click();
    cy.wait('@favoriteData');
    cy.wait(1000);
    // Make sure it's all with favorite items
    cy.get('[data-testid="icon-favorite"]').should(
      'have.css',
      'color',
      'rgb(229, 62, 62)'
    );

    //   Page 1 should be the default selected
    cy.get('[data-testid="pagination-1"]').should(
      'have.css',
      'color',
      'rgb(72, 187, 120)'
    );
  });
  it('Validate - small screen pagination', () => {
    cy.viewport('iphone-6');
    cy.get('[data-testid="pagination-short"').should('have.text', '1 of 16');

    // Change page
    cy.get('[data-testid="next-page"]').click();
    cy.url({ timeout: 1000 }).should('include', 'page=2');
    cy.get('[data-testid="pagination-short"').should('have.text', '2 of 16');
  });
});
