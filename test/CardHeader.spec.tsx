import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CardHeader from '../app/components/CardHeader/CardHeader';

describe('CardHeader component', () => {
  it('loads correct name and id', () => {
    const screen = render(
      <CardHeader
        id="001"
        name="pokemon-name"
        isFavorited={false}
        setIsFavorited={() => console.log('click favorite')}
        mainType="green"
      />
    );

    expect(screen.getByText('pokemon-name')).toBeInTheDocument();
    expect(screen.getByText('001')).toBeInTheDocument();
  });
  it('click in favorite icon', async () => {
    const setFavoriteMock = jest.fn();

    const screen = render(
      <CardHeader
        id="001"
        name="pokemon-name"
        isFavorited={true}
        setIsFavorited={setFavoriteMock}
        mainType="green"
      />
    );

    await userEvent.click(screen.getByTestId('detail-icon-favorite'));

    expect(setFavoriteMock).toHaveBeenCalled();
  });
});
