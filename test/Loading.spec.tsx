import { render } from '@testing-library/react';
import Loading from '../app/components/Loading/Loading';

describe('Loading component', () => {
  it('test loading', () => {
    const screen = render(<Loading />);

    expect(screen.getByTestId('loading-spinner')).toBeInTheDocument();
  });
});
