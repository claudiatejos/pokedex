import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Evolutions from '../app/components/Evolutions/Evolutions';

const mockEvolutions = [
  { id: '003', name: 'evolutions-03', image: 'www.google.com' },
  { id: '004', name: 'evolutions-04', image: 'www.google.com' },
];

const mockPreviousEvolutions = [
  { id: '001', name: 'evolutions-01', image: 'www.google.com' },
];

describe('Evolutions components', () => {
  it('render component with evolutions only', () => {
    const mockNavigate = jest.fn();
    const screen = render(
      <Evolutions
        evolutions={mockEvolutions}
        previousEvolutions={[]}
        navigate={mockNavigate}
      />
    );

    expect(screen.getByText('Evolutions')).toBeInTheDocument();

    expect(screen.getByText(mockEvolutions[0].name)).toBeInTheDocument();

    expect(screen.queryByText('Previous evolutions')).not.toBeInTheDocument();
  });
  it('render component with evolutions and previous evolutions', () => {
    const mockNavigate = jest.fn();
    const screen = render(
      <Evolutions
        evolutions={mockEvolutions}
        previousEvolutions={mockPreviousEvolutions}
        navigate={mockNavigate}
      />
    );

    expect(screen.getByText('Evolutions')).toBeInTheDocument();

    expect(screen.getByText(mockEvolutions[0].name)).toBeInTheDocument();

    expect(screen.getByText('Previous evolutions')).toBeInTheDocument();
    expect(
      screen.getByText(mockPreviousEvolutions[0].name)
    ).toBeInTheDocument();
  });

  it('render component - only previous evolutions', () => {
    const mockNavigate = jest.fn();

    const screen = render(
      <Evolutions
        evolutions={[]}
        previousEvolutions={mockPreviousEvolutions}
        navigate={mockNavigate}
      />
    );

    expect(screen.getByText('Previous evolutions')).toBeInTheDocument();

    expect(
      screen.getByText(mockPreviousEvolutions[0].name)
    ).toBeInTheDocument();
    expect(screen.queryByText(mockEvolutions[0].name)).not.toBeInTheDocument();
  });
  it('render component - click in pokemon', async () => {
    const mockNavigate = jest.fn();

    const screen = render(
      <Evolutions
        evolutions={[]}
        previousEvolutions={mockPreviousEvolutions}
        navigate={mockNavigate}
      />
    );

    await userEvent.click(screen.getByText(mockPreviousEvolutions[0].name));
    expect(mockNavigate).toHaveBeenCalled();
  });
});
