import { useContext, useEffect } from 'react';

import { ChakraProvider, Box, Image } from '@chakra-ui/react';
import { withEmotionCache } from '@emotion/react';

import type { MetaFunction, LinksFunction } from '@remix-run/node';
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
} from '@remix-run/react';
import { ServerStyleContext, ClientStyleContext } from './context';
import { theme } from './chakraThemeExtension';

export const meta: MetaFunction = () => ({
  charset: 'utf-8',
  title: 'Pokedex app',
  viewport: 'width=device-width,initial-scale=1',
});

export const links: LinksFunction = () => {
  return [
    {
      rel: 'icon',
      href:
        'https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png',
      type: 'image/png',
    },
    { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
    { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
    {
      rel: 'stylesheet',
      href:
        'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap',
    },
  ];
};

interface DocumentProps {
  children: React.ReactNode;
}

const Document = withEmotionCache(
  ({ children }: DocumentProps, emotionCache) => {
    const serverStyleData = useContext(ServerStyleContext);
    const clientStyleData = useContext(ClientStyleContext);

    // Only executed on client
    useEffect(() => {
      // re-link sheet container
      emotionCache.sheet.container = document.head;
      // re-inject tags
      const tags = emotionCache.sheet.tags;
      emotionCache.sheet.flush();
      tags.forEach((tag) => {
        (emotionCache.sheet as any)._insertTag(tag);
      });
      // reset cache to reapply global styles
      clientStyleData?.reset();
    }, []);

    return (
      <html lang="en">
        <head>
          <Meta />
          <Links />
          {serverStyleData?.map(({ key, ids, css }) => (
            <style
              key={key}
              data-emotion={`${key} ${ids.join(' ')}`}
              dangerouslySetInnerHTML={{ __html: css }}
            />
          ))}
        </head>
        <body>
          {children}
          <ScrollRestoration />
          <Scripts />
          {/* For development live reload */}
          <LiveReload />
        </body>
      </html>
    );
  }
);

export default function App() {
  return (
    <Document>
      <ChakraProvider theme={theme}>
        {/* To render child routing */}
        <Outlet />
      </ChakraProvider>
    </Document>
  );
}
export function CatchBoundary() {
  const caught = useCatch();

  if (caught.status === 404) {
    return (
      <>
        <Document>
          <ChakraProvider theme={theme}>
            <Box display="flex" justifyContent="center">
              <Box fontSize={['5rem', '10rem']}>4</Box>
              <Image
                src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
                alt="Pokeball"
                align="center"
                maxHeight={['6rem', '10rem']}
                alignSelf="center"
              />
              <Box fontSize={['5rem', '10rem']}>4</Box>
            </Box>
            <Box
              textAlign="center"
              fontSize="1.5rem"
              fontWeight="semibold"
              color="gray.500"
            >
              Page not found.
            </Box>
          </ChakraProvider>
        </Document>
      </>
    );
  }
}
