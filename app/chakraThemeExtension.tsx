import { extendTheme } from '@chakra-ui/react';

export const theme = extendTheme({
  colors: {
    flying: {
      100: '#90CDF4',
    },
    ground: {
      100: '#dfbf7e',
    },
    fighting: {
      100: '#c7a1bb',
    },
    psychic: {
      100: '#D6BCFA',
    },
    rock: {
      100: '#ffc78e',
    },
    steel: {
      100: '#A0AEC0',
    },
    ghost: {
      100: '#c2a6ff',
    },
    dragon: {
      100: '#a382ff',
    },
  },
});
