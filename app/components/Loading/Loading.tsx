import { Box, Spinner } from '@chakra-ui/react';
export default function Loading() {
  return (
    <Box
      left="0"
      top="0"
      width="100%"
      height="100%"
      position="fixed"
      background="#22222269"
      zIndex="1"
      data-testid="loading-spinner"
    >
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
        left="50%"
        position="absolute"
        top="50%"
      />
    </Box>
  );
}
