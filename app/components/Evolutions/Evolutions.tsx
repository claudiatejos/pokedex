import { Box, Flex, Center, Image, Icon } from '@chakra-ui/react';
import { FaArrowRight } from 'react-icons/fa';
import type { EvolutionsData } from '~/types';

interface EvolutionsProps {
  evolutions: Array<EvolutionsData>;

  previousEvolutions: Array<EvolutionsData>;
  navigate: (path: string) => void;
}

/**
 * Componet to handle Evolutions/Previous evolutions
 */
export default function Evolutions({
  evolutions,
  previousEvolutions,
  navigate,
}: EvolutionsProps) {
  const evolutionsSection = (
    title: string,
    evolutions: Array<EvolutionsData>
  ) => {
    return (
      <div>
        <Box
          as="p"
          color="gray.600"
          fontSize={['sm', 'md', '2rem']}
          fontWeight="semibold"
          mr="1rem"
          mb="1rem"
          textAlign="center"
        >
          {title}
        </Box>
        <Flex justifyContent="center">
          {evolutions.map((evolution, idx) => (
            <Flex
              key={`${evolution.id}-${evolution.name}`}
              data-testid={`${evolution.id}-${evolution.name}`}
            >
              <Center
                flexDirection={['column']}
                onClick={() => navigate(`/${evolution.id}`)}
                cursor="pointer"
              >
                <Box
                  as="span"
                  color="gray.600"
                  fontSize={['sm', 'md', 'lg']}
                  fontWeight="semibold"
                  mr="1rem"
                >
                  {evolution.name}
                </Box>
                <Image
                  src={evolution.image}
                  alt={evolution.name}
                  align="center"
                  maxHeight={['auto', '15rem']}
                  margin="auto"
                />
              </Center>
              {idx !== evolutions.length - 1 ? (
                <Icon
                  as={FaArrowRight}
                  h="2rem"
                  w="2rem"
                  alignSelf="center"
                  marginLeft="2rem"
                  marginRight="2rem"
                />
              ) : null}
            </Flex>
          ))}
        </Flex>
      </div>
    );
  };

  return (
    <Box
      display={
        previousEvolutions.length > 0 && evolutions.length > 0
          ? 'flex'
          : 'block'
      }
      justifyContent={
        previousEvolutions.length > 0 && evolutions.length > 0 ? 'center' : ''
      }
    >
      {/* Previous evolutions sections */}
      {previousEvolutions.length > 0
        ? evolutionsSection(
            'Previous evolutions',
            previousEvolutions?.sort((a, b) => +a.id - +b.id)
          )
        : null}

      {/* Evolutions section */}
      {evolutions.length > 0
        ? evolutionsSection('Evolutions', evolutions)
        : null}
    </Box>
  );
}
