import { Box, Flex, useMediaQuery } from '@chakra-ui/react';
import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';

interface PaginationProps {
  limit: number;
  totalCount: number;
  onClick: (currentPage: number) => void;
  currentPage: number;
}
/**
 * Pagination component
 */
export default function Pagination({
  limit,
  totalCount,
  onClick,
  currentPage,
}: PaginationProps) {
  const [isLargerThan710] = useMediaQuery('(min-width: 710px)');

  const pagesQuantity = Math.ceil(totalCount / limit);

  return (
    <Flex justifyContent="center" mt="2rem" pb="2rem">
      {/* Back button */}
      <Box
        p="0.75rem"
        as="span"
        cursor={currentPage === 1 ? 'not-allowed' : 'pointer'}
        bg="gray.100"
        border="1px"
        borderColor="gray.300"
        borderLeftRadius="lg"
        _hover={
          currentPage !== 1
            ? {
                background: 'white',
                color: 'green.500',
              }
            : {}
        }
        onClick={() => (currentPage !== 1 ? onClick(currentPage - 1) : {})}
        data-testid="previous-page"
      >
        <ChevronLeftIcon />
      </Box>

      {/* Bigger screen it will render pages quantity otherwise just the current page of <quantitty> */}
      {isLargerThan710 ? (
        [...Array(pagesQuantity).keys()].map((page) => (
          <Box
            key={`page-${page + 1}`}
            p="0.75rem"
            as="span"
            color={currentPage === page + 1 ? 'green.400' : 'gray.500'}
            cursor="pointer"
            bg={currentPage === page + 1 ? 'white' : 'gray.100'}
            border="1px"
            borderColor="gray.300"
            _hover={{
              background: 'white',
              color: 'green.500',
            }}
            onClick={() => onClick(page + 1)}
            data-testid={`pagination-${page + 1}`}
          >
            {page + 1}
          </Box>
        ))
      ) : (
        <Box
          key={`page-${currentPage}`}
          p="0.75rem"
          as="span"
          color={'green.400'}
          cursor="pointer"
          bg={'white'}
          border="1px"
          borderColor="gray.300"
          _hover={{
            background: 'white',
            color: 'green.500',
          }}
          onClick={() => onClick(currentPage + 1)}
          data-testid={'pagination-short'}
        >
          {currentPage} of {pagesQuantity}
        </Box>
      )}

      {/* Next button */}
      <Box
        p="0.75rem"
        as="span"
        bg="gray.100"
        border="1px"
        borderColor="gray.300"
        borderRightRadius="lg"
        _hover={
          currentPage !== pagesQuantity
            ? {
                background: 'white',
                color: 'green.500',
              }
            : {}
        }
        onClick={() =>
          currentPage !== pagesQuantity ? onClick(currentPage + 1) : {}
        }
        cursor={currentPage === pagesQuantity ? 'not-allowed' : 'pointer'}
        data-testid="next-page"
      >
        <ChevronRightIcon />
      </Box>
    </Flex>
  );
}
