import { Box, Badge, Icon } from '@chakra-ui/react';
import { MdOutlineFavoriteBorder, MdOutlineFavorite } from 'react-icons/md';
import { TypeColor } from '../../types';

interface CardHeaderProps {
  id: string;
  name: string;
  isFavorited: boolean;
  setIsFavorited: (isFavorited: boolean) => void;
  mainType: string;
}

/**
 * Component for Card details header
 */
export default function CardHeader({
  id,
  name,
  isFavorited,
  setIsFavorited,
  mainType,
}: CardHeaderProps) {
  return (
    <Box
      display="flex"
      alignItems="center"
      pb="1rem"
      justifyContent="space-between"
    >
      {/* type */}
      <Badge
        borderRadius="full"
        px="2"
        variant="subtle"
        colorScheme={TypeColor[mainType as keyof typeof TypeColor]}
        fontSize={['sm', 'md']}
      >
        {id}
      </Badge>

      {/* name */}
      <Box
        color="gray.500"
        fontWeight="semibold"
        letterSpacing="wide"
        fontSize={['sm', 'md']}
        textTransform="uppercase"
        ml="2"
      >
        {name}
      </Box>

      {/* favorite icon */}
      <Icon
        as={isFavorited ? MdOutlineFavorite : MdOutlineFavoriteBorder}
        ml="2"
        color={isFavorited ? 'red.500' : 'gray.600'}
        cursor="pointer"
        onClick={() => setIsFavorited(!isFavorited)}
        h="1.5rem"
        w="1.5rem"
        data-testid="detail-icon-favorite"
      />
    </Box>
  );
}
