import type { LoaderFunction, ActionArgs } from '@remix-run/node';
import { json } from '@remix-run/node';
import {
  useLoaderData,
  useSubmit,
  useTransition,
  useCatch,
  useNavigate,
} from '@remix-run/react';
import {
  Box,
  Image,
  Flex,
  Divider,
  Tag,
  TagLabel,
  Center,
  Icon,
  IconButton,
} from '@chakra-ui/react';
import invariant from 'tiny-invariant';
import { GiWeight, GiBodyHeight } from 'react-icons/gi';
import { FaHeartbeat } from 'react-icons/fa';
import { BsLightningCharge } from 'react-icons/bs';
import { AiFillSound } from 'react-icons/ai';
import { ArrowBackIcon } from '@chakra-ui/icons';
import Evolutions from '~/components/Evolutions/Evolutions';
import CardHeader from '~/components/CardHeader/CardHeader';
import Loading from '~/components/Loading/Loading';
import type { EvolutionsData, PokemonStorageProps } from '~/types';
import { TypeColor } from '~/types';
import { getSession, commitSession } from '~/sessions';

type LoaderData = {
  id: string;
  name: string;
  sound: string;
  image: string;
  weight?: {
    minimum: number;
    maximum: number;
  };
  height?: {
    minimum: number;
    maximum: number;
  };
  maxHP: number;
  maxCP: number;
  types: Array<string>;
  evolutions: Array<EvolutionsData>;
  previousEvolutions: Array<EvolutionsData>;
  isFavorite: boolean;
};

/**
 * Loader function - used for loading the pokemon current data
 * @param params.pokemonId -> pokemond id from url path param
 * @returns pokemons payload
 */
export const loader: LoaderFunction = async ({ request, params }) => {
  invariant(params.pokemonId, 'pokemonId not found');

  // get session data
  const session = await getSession(request.headers.get('Cookie'));
  const sessionData = session.get('favorites');

  // try on fetching pokemon
  try {
    const response = await fetch(
      `https://q-exercise-api.o64ixruq9hj.us-south.codeengine.appdomain.cloud/api/rest/pokemon/${params.pokemonId}`
    );

    const data = await response.json();

    // set favorite or not based on the session if there's any, otherwise use API data
    const pokemonData = sessionData?.find(
      (session: PokemonStorageProps) => session.id === params.pokemonId
    )
      ? {
          ...data,
          isFavorite: Boolean(
            sessionData.find(
              (session: PokemonStorageProps) => session.id === params.pokemonId
            )?.isFavorite ?? data.isFavorite
          ),
        }
      : data;

    return json(pokemonData, {
      status: 200,
      headers: {
        'Set-Cookie': await commitSession(session),
      },
    });

    // simulate not data found/ error in API
    // throw new Error('something went wrong');
  } catch (error: any) {
    console.error(error?.message);

    throw new Response('Not Found', {
      status: 404,
    });
  }
};

/**
 * Action function - called when form action submittions is triggered
 * This function will favortie/unfavorite the pokemond based on it's id
 * @param request - form request
 * @returns - pokemon data updated
 */
export async function action({ request }: ActionArgs) {
  // get session data
  const session = await getSession(request.headers.get('Cookie'));
  const sessionData = session.get('favorites');

  // get data from form
  const formData = await request.formData();
  const isFavorited = formData.get('isFavorited');
  const id = formData.get('id');

  const baseUrl =
    'https://q-exercise-api.o64ixruq9hj.us-south.codeengine.appdomain.cloud/api/rest/pokemon/';

  const fetchOptions = {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
  };

  if (isFavorited === 'true') {
    const response = await fetch(`${baseUrl}${id}/favorite`, fetchOptions);
    const data = await response.json();

    // update session data
    // from current session filter items
    // update session item with content
    session.set(
      'favorites',
      [
        ...(sessionData ?? [])?.filter(
          (actual: PokemonStorageProps) => actual.id !== id
        ),
        ...[
          {
            id,
            isFavorite: isFavorited,
            name: data.name,
            image: data.image,
            types: data.types,
          },
        ],
      ].slice(0, 22)
    );

    return json(
      { data, message: 'Successfully updated' },
      {
        status: 200,
        headers: {
          'Set-Cookie': await commitSession(session),
        },
      }
    );
  } else {
    const response = await fetch(`${baseUrl}${id}/unfavorite`, fetchOptions);
    const data = await response.json();

    // update session data
    // filter the one being unfavorited
    session.set('favorites', [
      ...(sessionData ?? [])?.filter(
        (actual: PokemonStorageProps) => actual.id !== id
      ),
    ]);
    return json(
      { data, message: 'Successfully updated' },
      {
        status: 200,
        headers: {
          'Set-Cookie': await commitSession(session),
        },
      }
    );
  }
}

export default function PokemonDetail() {
  const data = useLoaderData() as LoaderData;
  const submit = useSubmit();
  const transition = useTransition();
  const navigate = useNavigate();

  const isUpdatingFavorite = Boolean(transition.submission);

  return (
    <Box bg="gray.50" h="100vh" p="1rem">
      <IconButton
        mb="2rem"
        colorScheme="gray"
        aria-label="Back"
        fontSize="20px"
        icon={<ArrowBackIcon />}
        onClick={() => navigate('/')}
        h="2rem"
        w="2rem"
        data-testid="back-button"
      />

      {isUpdatingFavorite ? <Loading /> : null}
      <Box
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        bg="white"
        p="1rem"
      >
        {/* First row section */}
        <CardHeader
          id={data.id}
          name={data.name}
          isFavorited={data.isFavorite}
          mainType={data.types[0]}
          setIsFavorited={(isFavorited) => {
            const formData = new FormData();
            formData.append('isFavorited', isFavorited.toString());
            formData.append('id', data.id);

            submit(formData, { method: 'post' });
          }}
        />
        {/* Image section */}
        <Flex justifyContent="center">
          <Center flexDirection={['column', 'row']}>
            <Icon
              as={AiFillSound}
              cursor="pointer"
              onClick={() => {
                const audio = new Audio(data.sound);
                audio.play();
              }}
              h="2rem"
              w="2rem"
              alignSelf="baseline"
            />
            <Image
              src={data.image}
              alt={data.name}
              align="center"
              maxHeight={['auto', '15rem']}
              margin="auto"
            />
          </Center>
        </Flex>
        <Divider orientation="horizontal" mt="1rem" mb="1rem" />
        {/* Extra info about pokemon */}
        <Flex justifyContent="space-between">
          <Center flexDirection={['column', 'row']}>
            <Icon as={GiWeight} h={['1.5rem', '2rem']} w={['1.5rem', '2rem']} />
            <Box
              as="span"
              color="gray.600"
              fontWeight="semibold"
              fontSize={['sm', 'md']}
              ml="1"
            >
              {data?.weight?.minimum} - {data?.weight?.maximum}
            </Box>
          </Center>

          <Center flexDirection={['column', 'row']}>
            <Icon
              as={GiBodyHeight}
              h={['1.5rem', '2rem']}
              w={['1.5rem', '2rem']}
            />

            <Box
              as="span"
              color="gray.600"
              fontWeight="semibold"
              fontSize={['sm', 'md']}
              ml="1"
            >
              {data?.height?.minimum} - {data?.height?.maximum}
            </Box>
          </Center>
          <Center flexDirection={['column', 'row']}>
            <Icon
              as={FaHeartbeat}
              h={['1.5rem', '2rem']}
              w={['1.5rem', '2rem']}
              color="red.500"
            />
            <Box
              as="span"
              color="gray.600"
              fontWeight="semibold"
              fontSize={['sm', 'md']}
              ml="1"
            >
              {data.maxHP}
            </Box>
          </Center>
          <Center flexDirection={['column', 'row']}>
            <Icon
              as={BsLightningCharge}
              h={['1.5rem', '2rem']}
              w={['1.5rem', '2rem']}
              color="yellow.500"
            />
            <Box
              as="span"
              color="gray.600"
              fontWeight="semibold"
              fontSize={['sm', 'md']}
              ml="1"
            >
              {data.maxCP}
            </Box>
          </Center>
        </Flex>

        {/* Pokemon types */}
        <Flex mt="2rem">
          <Box
            as="p"
            color="gray.600"
            fontSize={['sm', 'md', 'lg']}
            fontWeight="semibold"
            mr="1rem"
          >
            Types
          </Box>
          {data.types.map((type) => (
            <Tag
              size={['md', 'lg']}
              key={type}
              variant="subtle"
              cursor="default"
              colorScheme={TypeColor[type as keyof typeof TypeColor]}
              mr="0.75rem"
            >
              <TagLabel>{type}</TagLabel>
            </Tag>
          ))}
        </Flex>

        <Divider orientation="horizontal" mt="1rem" mb="1rem" />

        {data.evolutions.length > 0 || data.previousEvolutions.length > 0 ? (
          <Evolutions
            evolutions={data.evolutions}
            previousEvolutions={data.previousEvolutions}
            navigate={navigate}
          />
        ) : null}
      </Box>
    </Box>
  );
}

/** Error boundary - error handler for server/client errors */
export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);

  return (
    <>
      <Box display="flex" justifyContent="center">
        <Box fontSize="10rem">5</Box>
        <Image
          src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
          alt="Pokeball"
          align="center"
          maxHeight={['auto', '10rem']}
          alignSelf="center"
        />
        <Image
          src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
          alt="Pokeball"
          align="center"
          maxHeight={['auto', '10rem']}
          alignSelf="center"
        />
      </Box>
      <Box
        textAlign="center"
        fontSize="1.5rem"
        fontWeight="semibold"
        color="gray.500"
      >
        Something went wrong, try again.
      </Box>
    </>
  );
}

/**
 * Catch boundary - Response type is thrown document isn't found on a web server
 */
export function CatchBoundary() {
  const caught = useCatch();

  if (caught.status === 404) {
    return (
      <>
        <Box display="flex" justifyContent="center">
          <Box fontSize="10rem">4</Box>
          <Image
            src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
            alt="Pokeball"
            align="center"
            maxHeight={['auto', '10rem']}
            alignSelf="center"
          />
          <Box fontSize="10rem">4</Box>
        </Box>
        <Box
          textAlign="center"
          fontSize="1.5rem"
          fontWeight="semibold"
          color="gray.500"
        >
          No pokemon found
        </Box>
      </>
    );
  }

  throw new Error(`Unexpected caught response with status: ${caught.status}`);
}
