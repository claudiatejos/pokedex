import { useState, useEffect } from 'react';
import type { LoaderFunction, ActionArgs } from '@remix-run/node';
import { json } from '@remix-run/node';
import {
  useLoaderData,
  useSearchParams,
  useLocation,
  useNavigate,
  useTransition,
  useFetcher,
  useCatch,
} from '@remix-run/react';
import {
  Input,
  Flex,
  Select,
  Grid,
  GridItem,
  Image,
  Badge,
  Box,
  TableContainer,
  Table,
  Tbody,
  Tr,
  Td,
  Icon,
} from '@chakra-ui/react';
import {
  MdGridView,
  MdOutlineFormatListBulleted,
  MdOutlineFavoriteBorder,
  MdOutlineFavorite,
} from 'react-icons/md';
import Pagination from '~/components/Pagination/Pagination';
import Loading from '~/components/Loading/Loading';
import type {
  PokemonStorageProps,
  PokemonsItem,
  PokemonsFetchResponseType,
} from '~/types';
import { TypeColor } from '~/types';
import { getSession, commitSession } from '~/sessions';

import { useDebounce } from '~/hooks/useDebounce';

/**
 * Loader function triggered in GET of this page
 * This function will call the pokemon types, pokemons and if favorite will use session
 * Returns obj {pokemonTypes: <array>, data: <pokemons get response>}
 */
export const loader: LoaderFunction = async ({ request }) => {
  //  get session data
  const session = await getSession(request.headers.get('Cookie'));
  const sessionData = session.get('favorites');

  //  get request params
  const url = new URL(request.url);
  const searchParam = url.searchParams.get('search');
  const typeParam = url.searchParams.get('type');
  const pageParam = url.searchParams.get('page');
  const isFavorite = Boolean(url.searchParams.get('isFavorite'));

  // Make types fetch
  const responseTypesRequest = await fetch(
    'https://q-exercise-api.o64ixruq9hj.us-south.codeengine.appdomain.cloud/api/rest/pokemon-types'
  );
  const pokemonTypes = await responseTypesRequest.json();

  // Build url for fetch pokemons
  const baseURL = new URL(
    'https://q-exercise-api.o64ixruq9hj.us-south.codeengine.appdomain.cloud/api/rest/pokemon'
  );
  // add query params to url
  if (searchParam) baseURL.searchParams.append('search', searchParam);
  if (typeParam) baseURL.searchParams.append('type', typeParam);
  if (pageParam)
    baseURL.searchParams.append('offset', `${(Number(pageParam) - 1) * 10}`);
  // if (isFavorite)
  //   baseURL.searchParams.append('isFavorite', isFavorite.toString());

  const response = await fetch(baseURL);

  const data = await response.json();

  if (data.items.some((i: PokemonsItem) => i.isFavorite)) {
    // filter from the API data the ones that are favorite and dont exist in session data so can set it
    const favoritesToSet: Array<PokemonsItem> = data.items.filter(
      (i: PokemonsItem) =>
        i.isFavorite &&
        !sessionData?.find(
          (sessionItem: PokemonStorageProps) => sessionItem.id === i.id
        )
    );

    const setInSession = [
      ...(sessionData ?? []),
      ...(favoritesToSet
        ? favoritesToSet.map((i) => ({
            id: i.id,
            isFavorite: i.isFavorite,
            name: i.name,
            image: i.image,
            types: i.types,
          }))
        : []),
    ];
    // cookie max around 22 items
    session.set('favorites', setInSession.slice(0, 22));
  }

  // due to APIs inconsistency we will use session as source of truth for users favorite pokemons
  if (isFavorite) {
    let dataItems = sessionData;

    // sort session data
    dataItems = dataItems?.sort(
      (a: PokemonStorageProps, b: PokemonStorageProps) => +a.id - +b.id
    );

    if (searchParam && dataItems)
      dataItems = dataItems?.filter((session: PokemonStorageProps) =>
        session.name.toLowerCase().includes(searchParam.toLowerCase())
      );
    if (typeParam && dataItems)
      dataItems = dataItems.filter((session: PokemonStorageProps) =>
        session.types.includes(typeParam)
      );
    if (pageParam && dataItems) {
      // our limit is always 10 items per page
      const pageOffSet = (Number(pageParam) - 1) * 10;
      dataItems =
        dataItems.length - pageOffSet > 10
          ? dataItems.slice(pageOffSet, 10)
          : dataItems.slice(pageOffSet);
    }

    data.count =
      !searchParam && !typeParam
        ? sessionData?.length ?? 0
        : dataItems?.length ?? 0;

    data.items = dataItems ?? [];
  } else {
    // update favorite from session
    data.items = data.items.map((i: PokemonsItem) =>
      sessionData?.find((session: PokemonStorageProps) => session.id === i.id)
        ? {
            ...i,
            isFavorite: Boolean(
              sessionData.find(
                (session: PokemonStorageProps) => session.id === i.id
              )?.isFavorite
            ),
          }
        : i
    );
  }

  return json(
    { pokemonTypes, data },
    {
      status: 200,
      headers: {
        'Set-Cookie': await commitSession(session),
      },
    }
  );
};

/**
 * Action function - called when form action submittions is triggered
 * This function will favorite/unfavorite the pokemond based on it's id
 * @param request - form request
 * @returns - pokemon data updated
 */
export async function action({ request }: ActionArgs) {
  // get session data
  const session = await getSession(request.headers.get('Cookie'));
  const sessionData = session.get('favorites');

  // get data from form
  const formData = await request.formData();
  const isFavorite = formData.get('isFavorite');
  const id = formData.get('id');

  // basic POST url
  const baseUrl =
    'https://q-exercise-api.o64ixruq9hj.us-south.codeengine.appdomain.cloud/api/rest/pokemon/';

  // basic POST options
  const fetchOptions = {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
  };

  if (isFavorite === 'true') {
    const response = await fetch(`${baseUrl}${id}/favorite`, fetchOptions);
    const data = await response.json();

    // update session data
    // from current session filter items
    // update session item with content
    session.set(
      'favorites',
      [
        ...(sessionData ?? [])?.filter(
          (actual: PokemonStorageProps) => actual.id !== id
        ),
        ...[
          {
            id,
            isFavorite,
            name: data.name,
            image: data.image,
            types: data.types,
          },
        ],
      ].slice(0, 22)
    );
    return json(
      { data, message: 'Successfully updated' },
      {
        status: 200,
        headers: {
          'Set-Cookie': await commitSession(session),
        },
      }
    );
  } else {
    const response = await fetch(`${baseUrl}${id}/unfavorite`, fetchOptions);
    const data = await response.json();

    // update session data
    // filter the one being unfavorited
    session.set('favorites', [
      ...(sessionData ?? [])?.filter(
        (actual: PokemonStorageProps) => actual.id !== id
      ),
    ]);

    return json(
      { data, message: 'Successfully updated' },
      {
        status: 200,
        headers: {
          'Set-Cookie': await commitSession(session),
        },
      }
    );
  }
}

export default function Pokedex() {
  const { pokemonTypes, data } = useLoaderData() as PokemonsFetchResponseType;

  const location = useLocation();
  const fetcher = useFetcher();
  const navigate = useNavigate();
  const transition = useTransition();
  const [searchParam, setSearchParam] = useSearchParams();

  const [isGridActive, setIsGridActive] = useState<boolean>(true);
  const [isAllActive, setIsAllActive] = useState<boolean>(
    !Boolean(searchParam.get('isFavorite')) ?? true
  );
  const [searchInput, setSearchInput] = useState<string>();
  const [debouncedQuery] = useDebounce(searchInput, 500);
  const [currentPage, setCurrentPage] = useState<number>(
    searchParam.get('page') ? Number(searchParam.get('page')) : 1
  );

  useEffect(() => {
    const param = new URLSearchParams(location.search);

    if (!!searchInput) {
      param.set('search', searchInput);
      param.set('page', '1');
    } else {
      param.delete('search');
    }

    setSearchParam(param);
  }, [debouncedQuery]);

  useEffect(() => {
    setCurrentPage(
      searchParam.get('page') ? Number(searchParam.get('page')) : 1
    );
  }, [location]);

  const isLoading =
    Boolean(transition.submission) || transition.state === 'loading';

  return (
    <Box bg="gray.50" h="100vh" p="1rem">
      {/* All vs Favorite selection */}
      <Flex justifyContent="center" pb="1rem">
        <Box
          p="0.75rem"
          as="div"
          color={isAllActive ? 'green.400' : 'gray.500'}
          cursor="pointer"
          bg={isAllActive ? 'white' : 'gray.100'}
          border="1px"
          borderColor="gray.300"
          borderLeftRadius="lg"
          _hover={{
            background: 'white',
            color: 'green.500',
          }}
          onClick={() => {
            const param = new URLSearchParams(location.search);
            param.delete('isFavorite');
            setSearchParam(param);
            setIsAllActive((prevState) => !prevState);
          }}
          flex="1"
          textAlign="center"
          data-testid="all-pokemons"
        >
          All
        </Box>

        <Box
          p="0.75rem"
          as="div"
          color={!isAllActive ? 'green.400' : 'gray.500'}
          cursor="pointer"
          bg={!isAllActive ? 'white' : 'gray.100'}
          border="1px"
          borderColor="gray.300"
          borderRightRadius="lg"
          _hover={{
            background: 'white',
            color: 'green.500',
          }}
          onClick={() => {
            const param = new URLSearchParams(location.search);
            if (isAllActive) {
              param.set('isFavorite', 'true');
              param.set('page', '1');
            }
            setSearchParam(param);
            setIsAllActive((prevState) => !prevState);
            setCurrentPage(1);
          }}
          flex="1"
          textAlign="center"
          data-testid="favorite-pokemons"
        >
          Favorites
        </Box>
      </Flex>
      {/* header section */}

      <Flex justifyContent="space-between" pb="1rem" alignItems="center">
        <div style={{ flex: '1' }}>
          <Flex>
            <Input
              bg="white"
              type="search"
              name="search"
              mr="1rem"
              placeholder="Search for a pokemon"
              defaultValue={searchParam.get('search') ?? ''}
              onChange={(e) => setSearchInput(e.target.value)}
              data-testid="search-input"
            />
          </Flex>
        </div>

        <Select
          cursor="pointer"
          bg="white"
          flex="1"
          placeholder="Select type"
          defaultValue={searchParam.get('type') ?? ''}
          onChange={(e) => {
            const param = new URLSearchParams(location.search);

            if (!!e.target.value) {
              param.set('type', e.target.value);
              param.set('page', '1');
            } else {
              param.delete('type');
            }

            setSearchParam(param);
          }}
          data-testid="select-input"
        >
          {pokemonTypes?.map((type) => (
            <option
              key={type}
              value={type}
              data-testid={`${type}-select-input`}
            >
              {type}
            </option>
          ))}
        </Select>

        <Flex ml="1rem">
          <Box
            p="0.75rem"
            as="span"
            color={isGridActive ? 'green.400' : 'gray.500'}
            cursor="pointer"
            bg={isGridActive ? 'white' : 'gray.100'}
            border="1px"
            borderColor="gray.300"
            borderLeftRadius="lg"
            _hover={{
              background: 'white',
              color: 'green.500',
            }}
            onClick={() => setIsGridActive((prevState) => !prevState)}
            pt="0.5rem"
            pb="0.5rem"
            data-testid="show-grid-view"
          >
            <MdGridView style={{ height: '1.5rem', width: '1.5rem' }} />
          </Box>

          <Box
            p="0.75rem"
            as="span"
            color={!isGridActive ? 'green.400' : 'gray.500'}
            cursor="pointer"
            bg={!isGridActive ? 'white' : 'gray.100'}
            border="1px"
            borderColor="gray.300"
            borderRightRadius="lg"
            _hover={{
              background: 'white',
              color: 'green.500',
            }}
            onClick={() => setIsGridActive((prevState) => !prevState)}
            pt="0.5rem"
            pb="0.5rem"
            data-testid="show-list-view"
          >
            <MdOutlineFormatListBulleted
              style={{ height: '1.5rem', width: '1.5rem' }}
            />
          </Box>
        </Flex>
      </Flex>

      <main>
        {data?.count > 0 ? (
          <>
            {isLoading ? <Loading /> : null}

            {isGridActive && data?.items.length > 0 ? (
              <Grid
                templateColumns={{
                  base: 'repeat(1, 1fr)',
                  sm: 'repeat(2, 1fr)',
                  md: 'repeat(4, 1fr)',
                  lg: 'repeat(5, 1fr)',
                }}
                gap={6}
              >
                {data?.items
                  ?.filter((item) => (!isAllActive ? item.isFavorite : item))
                  ?.map((item) => (
                    <GridItem
                      data-testid="grid-item"
                      key={item.id}
                      maxW="sm"
                      maxH="md"
                      borderWidth="1px"
                      borderRadius="lg"
                      overflow="hidden"
                      bg="white"
                      p="1rem"
                      cursor="pointer"
                      _hover={{
                        borderColor: `${
                          TypeColor[item.types[0] as keyof typeof TypeColor]
                        }.500`,
                        borderWidth: '2px',
                      }}
                      onClick={() => navigate(`/${item.id}`)}
                    >
                      <Box
                        display="flex"
                        alignItems="center"
                        pb="1rem"
                        justifyContent="space-between"
                      >
                        <Badge
                          borderRadius="full"
                          px="2"
                          variant="subtle"
                          colorScheme={
                            TypeColor[item.types[0] as keyof typeof TypeColor]
                          }
                        >
                          {item.id}
                        </Badge>
                        <Box
                          color="gray.500"
                          fontWeight="semibold"
                          letterSpacing="wide"
                          fontSize="xs"
                          textTransform="uppercase"
                          ml="2"
                        >
                          {item.name}
                        </Box>

                        <Icon
                          as={
                            item.isFavorite
                              ? MdOutlineFavorite
                              : MdOutlineFavoriteBorder
                          }
                          ml="2"
                          color={item.isFavorite ? 'red.500' : 'gray.600'}
                          cursor="pointer"
                          onClick={(e) => {
                            e.stopPropagation();
                            const isPokemonFavorited = !item.isFavorite;

                            const formData = new FormData();
                            formData.append(
                              'isFavorite',
                              isPokemonFavorited.toString()
                            );
                            formData.append('id', item.id);

                            fetcher.submit(formData, {
                              method: 'post',
                            });
                          }}
                          h="1.5rem"
                          w="1.5rem"
                          data-testid="icon-favorite"
                        />
                      </Box>
                      {/* image */}
                      <Image
                        src={item.image}
                        alt={item.name}
                        align="center"
                        maxHeight={['auto', '15rem']}
                        margin="auto"
                      />
                    </GridItem>
                  ))}
              </Grid>
            ) : !isGridActive && data?.items.length > 0 ? (
              <TableContainer>
                <Table variant="simple">
                  <Tbody bg="white">
                    {data?.items
                      ?.filter((item) =>
                        !isAllActive ? item.isFavorite : item
                      )
                      ?.map((item) => (
                        <Tr
                          key={item.id}
                          data-testid="list-item"
                          cursor="pointer"
                          onClick={() => navigate(`/${item.id}`)}
                        >
                          <Td w={['100%', '7rem']} p={['0.75rem', '1rem']}>
                            <Image
                              src={item.image}
                              alt={item.name}
                              align="center"
                              boxSize={['auto', '4rem']}
                            />
                          </Td>
                          <Td
                            isNumeric
                            display={{ base: 'none', sm: 'table-cell' }}
                            w="8rem"
                            color="gray.600"
                            fontWeight="semibold"
                            letterSpacing="wide"
                            fontSize={['xs', 'md']}
                            textTransform="uppercase"
                          >
                            <Badge
                              borderRadius="full"
                              px="2"
                              variant="subtle"
                              colorScheme={
                                TypeColor[
                                  item.types[0] as keyof typeof TypeColor
                                ]
                              }
                            >
                              {item.id}
                            </Badge>
                          </Td>
                          <Td
                            textAlign="center"
                            color="gray.600"
                            fontWeight="semibold"
                            letterSpacing="wide"
                            fontSize={['xs', 'md']}
                            textTransform="uppercase"
                          >
                            <Box>{item.name}</Box>
                            <Box
                              color="gray.500"
                              fontWeight="semibold"
                              letterSpacing="wide"
                              fontSize="xs"
                              textTransform="uppercase"
                              ml="2"
                            >
                              {item.types.map((type) => (
                                <span key={type}>{type} &bull; </span>
                              ))}
                            </Box>
                          </Td>
                          <Td
                            textAlign="end"
                            verticalAlign="middle"
                            h={['1rem', '1.5rem']}
                          >
                            <Icon
                              as={
                                item.isFavorite
                                  ? MdOutlineFavorite
                                  : MdOutlineFavoriteBorder
                              }
                              ml="2"
                              color={item.isFavorite ? 'red.500' : 'gray.600'}
                              cursor="pointer"
                              onClick={(e) => {
                                e.stopPropagation();
                                const isPokemonFavorited = !item.isFavorite;

                                const formData = new FormData();
                                formData.append(
                                  'isFavorite',
                                  isPokemonFavorited.toString()
                                );
                                formData.append('id', item.id);

                                fetcher.submit(formData, {
                                  method: 'post',
                                });
                              }}
                              h="1.5rem"
                              w="1.5rem"
                              data-testid="icon-favorite"
                            />
                          </Td>
                        </Tr>
                      ))}
                  </Tbody>
                </Table>
              </TableContainer>
            ) : null}
            <Pagination
              limit={data?.limit}
              totalCount={data?.count}
              currentPage={currentPage}
              onClick={(page) => {
                const param = new URLSearchParams(location.search);

                param.set('page', `${page}`);

                setSearchParam(param);
                setCurrentPage(page);
              }}
            />
          </>
        ) : (
          // No data fallback
          <>
            <Box display="flex" justifyContent="center">
              <Box fontSize={['5rem', '10rem']}>4</Box>
              <Image
                src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
                alt="Pokeball"
                align="center"
                maxHeight={['6rem', '10rem']}
                alignSelf="center"
              />
              <Box fontSize={['5rem', '10rem']}>4</Box>
            </Box>
            <Box
              textAlign="center"
              fontSize="1.5rem"
              fontWeight="semibold"
              color="gray.500"
            >
              Pokemon not found
            </Box>
          </>
        )}
      </main>
    </Box>
  );
}

/** Error boundary - error handler for server/client errors */
export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);

  return (
    <>
      <Box display="flex" justifyContent="center">
        <Box fontSize={['5rem', '10rem']}>5</Box>
        <Image
          src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
          alt="Pokeball"
          align="center"
          maxHeight={['6rem', '10rem']}
          alignSelf="center"
        />
        <Image
          src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
          alt="Pokeball"
          align="center"
          maxHeight={['6rem', '10rem']}
          alignSelf="center"
        />
      </Box>
      <Box
        textAlign="center"
        fontSize={['md', '1.5rem']}
        fontWeight="semibold"
        color="gray.500"
      >
        Something went wrong, try again.
      </Box>
    </>
  );
}

/**
 * Catch boundary - Response type is thrown document isn't found on a web server
 */
export function CatchBoundary() {
  const caught = useCatch();

  if (caught.status === 404) {
    return (
      <>
        <Box display="flex" justifyContent="center">
          <Box fontSize="10rem">5</Box>
          <Image
            src="https://www.freeiconspng.com/uploads/pokeball-transparent-png-2.png"
            alt="Pokeball"
            align="center"
            maxHeight={['auto', '10rem']}
            alignSelf="center"
          />
          <Box fontSize="10rem">4</Box>
        </Box>
        <Box
          textAlign="center"
          fontSize="1.5rem"
          fontWeight="semibold"
          color="gray.500"
        >
          This page doesnt seems to exist,
        </Box>
      </>
    );
  }
}
