export enum TypeColor {
  Grass = 'green',
  Poison = 'purple',
  Fire = 'red',
  Flying = 'flying',
  Water = 'blue',
  Bug = 'green',

  Normal = 'gray',
  Electric = 'yellow',
  Ground = 'ground',
  Fairy = 'pink',
  Fighting = 'fighting',
  Psychic = 'psychic',
  Rock = 'rock',
  Steel = 'steel',
  Ice = 'blue',
  Ghost = 'ghost',
  Dragon = 'dragon',
}

export type EvolutionsData = {
  id: string;
  name: string;
  image: string;
};

export interface PokemonStorageProps {
  id: string;
  name: string;
  image: string;
  types: Array<string>;
  isFavorite: boolean;
}

export interface PokemonsItem {
  id: string;
  number: number;
  name: string;
  image: string;
  isFavorite: boolean;

  types: Array<string>;
}

export interface PokemonsDataItems {
  limit: number;
  offset: number;
  count: number;
  items: Array<PokemonsItem>;
}

export type PokemonsFetchResponseType = {
  pokemonTypes: Array<string>;
  data: PokemonsDataItems;
};
